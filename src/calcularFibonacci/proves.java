package calcularFibonacci;



import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class proves {

	private calcul_Fibonacci valor1,valor2,valor3,valor4;

	@Before
	public void crear(){

		valor1 = new calcul_Fibonacci();
		valor2 = new calcul_Fibonacci();
		valor3 = new calcul_Fibonacci();
		valor4 = new calcul_Fibonacci();
		
	}

	
	@Test
	public void test() {

		Assert.assertEquals(0, valor1.calculFibonacci(0),0.005);
		Assert.assertEquals(1, valor2.calculFibonacci(1),0.500);
		Assert.assertEquals(55, valor3.calculFibonacci(10),0.005);
		
	}

	@Test (expected = IllegalArgumentException.class)
	public void test2(){
		
		System.out.println(valor4.calculFibonacci(-5));
		
	}

}

