package calcularFibonacci;

public class calcul_Fibonacci {

	public static void main(String[] args) {

		//calculFibonacci(10);

	}

	public int calculFibonacci(int n){

		if(n > 1){
			calcul_Fibonacci fiborecursive = new calcul_Fibonacci();

			return fiborecursive.calculFibonacci(n - 1) + fiborecursive.calculFibonacci(n - 2);

		}else if (n==1) {

	        return 1;

	    }else if (n==0){

	        return 0;

	    }else throw new IllegalArgumentException();
	}
}
